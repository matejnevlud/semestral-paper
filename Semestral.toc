\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {english}{}
\babel@toc {czech}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Seznam obr\'azk\r {u}}{3}{chapter*.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Seznam tabulek}{4}{chapter*.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Úvod}{5}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Popis problému}{5}{section.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Dosavadní způsoby klasifikace}{5}{section.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Klasifikace dokumentů}{6}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Návrh klasifikátoru}{6}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Metoda přenosu učení}{6}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Výběr konvoluční báze}{6}{subsection.2.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Extraktor rysů a klasifikátor}{7}{subsection.2.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.3}Doladění před-trénovaného modelu}{8}{subsection.2.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.3.1}Strategie 1 - Trénování celé sítě}{8}{subsubsection.2.2.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.3.2}Strategie 2 - Jemné ladění parametrů sítě}{8}{subsubsection.2.2.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.3.3}Strategie 3 - Zamražení konvoluční báze}{9}{subsubsection.2.2.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Implementace}{10}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Zdroj dat}{10}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}The RVL-CDIP Dataset}{10}{subsection.3.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Tobacco-3482}{11}{subsection.3.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Pracovní prostředí}{11}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Experimenty}{12}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Síť A}{12}{section.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Síť B}{12}{section.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Výsledky testování}{14}{section.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Grafy průběhu trénování}{14}{subsection.4.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}Srovnání s ostatními sítěmi}{15}{subsection.4.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Vylepšení}{16}{chapter.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Augmentace dat}{16}{section.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Zmenšit rozash jasů}{16}{section.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Zájmové body}{16}{section.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Velký malý dataset}{17}{section.5.4}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Závěr}{18}{chapter.6}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Literatura}{19}{chapter*.13}%
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {czech}{}
